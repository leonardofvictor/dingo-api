<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Facades;
use \Illuminate\Support\Facades\Facade;
/**
 * Description of User
 *
 * @author leonardo
 */
class UserFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'user';
    }
}
