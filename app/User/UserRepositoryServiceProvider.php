<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace User;
use Illuminate\Support\ServiceProvider;

/**
 * Description of UserServiceProvider
 *
 * @author leonardo
 */
class UserRepositoryServiceProvider extends ServiceProvider
{
    public function register(){
        $this->app->bind('User\UserRepositoryInterface','User\UserRepository');
    }

}
