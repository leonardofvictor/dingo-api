<?php
namespace User;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leonardo
 */
interface UserRepositoryInterface
{

    /*create a new user
     * 
     */
    public function persist($params);
    
    public function getById($id);
    
    public function getByEmail($email);
    
    public function getByName($name);
}
