<?php
use User\UserRepositoryInterface;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author leonardo
 */
class UserController extends BaseController
{
    public $users;
    
    public function __construct(UserRepositoryInterface $users)
    {
        $this->users = $users;
    }
    
    public function show($id = null) {
        if(!is_null($id)) {
            print_r($this->users->getById($id));
        }
    }
    
    public function store() {
        
        $params = Input::all();
        
        $this->users->persist($params);
    }
}
