<?php

class RegisterController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		try
        {
            
			$params     = Input::except('credential');
			$credential = Input::get('credential');
			
			$user   = User::setUserObject($params);
            
            if ( ! is_array($user)) 
            {
            	$user = $user->persist();

            	$cred = new UserCredential($credential['password'], $user->email);
              	$cred->user()->associate($user);
				$cred->save();
            	return Response::json(array('code' => 0, 'content' => $user->id, 'message' => 'ok'));
            }

            return Response::json(array('code' => 0, 'content' => array('error' => $user), 'message' => 'nok'));
        
        }
        catch (Exception $e) {
        	Response::json(array('code' => 1, 'content' => $e->getMessage(), 'message' => 'error'));
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
