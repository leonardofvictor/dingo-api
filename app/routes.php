<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//App::bind('User\UserRepositoryInterface','User\UserRepository');
Route::get('/', function()
{
	return View::make('hello');
});
Route::post('oauth/access_token', function()
{
    return AuthorizationServer::performAccessTokenFlow();
});
Route::resource('users','UserController');
