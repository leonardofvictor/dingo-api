<?php



class User extends Eloquent 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	
	public $timestamps = true;
    
    protected $dates = ['deleted_at'];
    
    protected $rules = array(
        'first_name'=>'required',
        'last_name'=>'required',
        'email'=>'required|email|unique:users',
        'birthday'=>'required|date_format:Y-m-d',
    );
    
    protected $fillable = array('first_name','last_name','email','gender','birthday');
    
    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $guarded = array('id','alias','piggme_code');
    
    public function credential() 
    {
        return $this->hasOne('UserCredential');
    }
    /**
	 * Saves the user and create his credentials.
	 *
	 * @var object
	 */
    public function persist()
    {
        try 
        {
            $this->save();
            return $this;
        } 
        catch (Exception $ex) 
        {
            throw $ex;
        }
    }
    /*
     * Validação de de campos da model
     *  
     */
    protected function isValid($input = null) {
        $fields = isset($input) ? $input : Input::all();
        return Validator::make($fields, $this->rules);
    }
    /**
	 * set the User object, based on the model's rules
	 *
	 * @var object
	 */
    public static function setUserObject($params)
    {
        if (isset($params)) 
         {
            
            $u = new User;
            
            $v = $u->isValid($params);
            if($v->passes()){
                $u->first_name  = $params['first_name'];
                $u->last_name   = $params['last_name'];
                $u->alias       = User::existsAlias(strtolower(trim($params['first_name'])) . '.' . strtolower(trim($params['last_name'])));
                $u->email       = $params['email'];
                $u->piggme_code = User::strRand(9,'numcusstr');
                $u->gender      = $params['gender'];
                $u->birthday    = $params['birthday'];

                return $u;
            }
            return $v->errors()->all();
        }
        return false;
    }
    /**
     * Creates an alias with the user's name and verifys the existence in the database.
     * if exists, generates a new alias and returns the alias permitted
     *
     * @var string
     */
    public static function existsAlias($alias) {
        $param = $alias;
        while(User::where('alias', '=', $param)->count()) {
           $param = $alias.User::strRand(4,'numeric'); 
        }
        
        return $param;
    }
    /**
    * Generate an random code
    *
    * @var string
    */
    public static function strRand($length = 9, $seeds = 'alphanum') {
		// Possible seeds
		$seedings['alpha'] = 'abcdefghijklmnopqrstuvwqyz';
		$seedings['numeric'] = '0123456789';
		$seedings['alphanum'] = 'abcdefghijklmnopqrstuvwqyz0123456789';
		$seedings['hexidec'] = '0123456789abcdef';
		$seedings['cusstr'] = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
		$seedings['capcusstr'] = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
		$seedings['numcusstr'] = '23456789';
	
		// Choose seed
		if (isset($seedings[$seeds]))
		{
			$seeds = $seedings[$seeds];
		}
	
		// Seed generator
		list($usec, $sec) = explode(' ', microtime());
		$seed = (float) $sec + ((float) $usec * 100000);
		mt_srand($seed);
	
		// Generate
		$str = '';
		$seeds_count = strlen($seeds);
	
		for ($i = 0; $length > $i; $i++)
		{
			$str .= $seeds{mt_rand(0, $seeds_count - 1)};
		}
	
		$data_code=$str;
	
		return $data_code;
	}

}
