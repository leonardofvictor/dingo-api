<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserCredential
 *
 * @author leonardo
 */
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class UserCredential extends Eloquent implements UserInterface
{
    use UserTrait;
    protected $hidden  = array('password');
    protected $guarded = array('password');
    protected $table = 'user_credentials';
    
    public function __construct($password = null, $email = null)
    {
        if(!empty($password) && !empty($email)) {
            $this->password = Hash::make($password.$email);
            $this->username	= $email;
        }
    	
    	parent::__construct();
    }
    /**
	 * Enables updated_at and created_at.
	 *
	 * @var boolean
	 */
    public $timestamps = true;
    
    public function user() {
        return $this->belongsTo('User');
    }
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->password;
    }
}
