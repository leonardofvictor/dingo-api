<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users',function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('user_affiliate_id');
            $table->string('piggme_code')->unique();
            $table->string('gender');
            $table->string('alias')->unique();
            $table->date('birthday');
            $table->timestamps();
            $table->softDeletes();
            $table->index('email');
        });
        Schema::create('user_credentials',function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('password');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
            $table->index('username');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
